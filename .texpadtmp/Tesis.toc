\select@language {spanish}
\contentsline {chapter}{Agradecimientos}{\es@scroman {ix}}{chapter*.1}
\contentsline {chapter}{Resumen}{\es@scroman {xi}}{chapter*.2}
\contentsline {chapter}{\numberline {1}Cap\IeC {\'\i }tulo I}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Introducci\'on}{1}{section.1.1}
\contentsline {section}{Notas bibliogr\'aficas}{1}{section.1.1}
\contentsline {section}{En el pr\'oximo cap\IeC {\'\i }tulo}{1}{section.1.1}
\contentsline {chapter}{\numberline {2}Cap\IeC {\'\i }tulo II}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Introducci\'on}{3}{section.2.1}
\contentsline {section}{Notas bibliogr\'aficas}{3}{section.2.1}
\contentsline {section}{En el pr\'oximo cap\IeC {\'\i }tulo}{3}{section.2.1}
\contentsline {chapter}{\numberline {3}PREGUNTAS DE INVESTIGACI\'ON}{5}{chapter.3}
\contentsline {section}{\numberline {3.1}Introducci\'on}{5}{section.3.1}
\contentsline {section}{Notas bibliogr\'aficas}{5}{section.3.1}
\contentsline {section}{En el pr\'oximo cap\IeC {\'\i }tulo}{5}{section.3.1}
\contentsline {chapter}{\numberline {4}METODOLOG\'IA DE INVESTIGACI\'ON}{7}{chapter.4}
\contentsline {section}{\numberline {4.1}Descripci\'on del experimento base}{8}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Factores y sus niveles}{8}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}Variables respuesta y m\'etricas}{9}{subsection.4.1.2}
\contentsline {subsection}{\numberline {4.1.3}Hip\'otesis Experimentales}{10}{subsection.4.1.3}
\contentsline {subsection}{\numberline {4.1.4}Covariables}{11}{subsection.4.1.4}
\contentsline {subsection}{\numberline {4.1.5}Objetos Experimentales.}{11}{subsection.4.1.5}
\contentsline {subsection}{\numberline {4.1.6}Dise\~no Experimental }{13}{subsection.4.1.6}
\contentsline {subsection}{\numberline {4.1.7}Selecci\'on de sujetos}{14}{subsection.4.1.7}
\contentsline {subsection}{\numberline {4.1.8}Asignaci\'on de los Sujetos a Tratamientos}{14}{subsection.4.1.8}
\contentsline {subsection}{\numberline {4.1.9}Objetos Experimentales}{15}{subsection.4.1.9}
\contentsline {subsection}{\numberline {4.1.10}Protocolo Experimental}{16}{subsection.4.1.10}
\contentsline {subsection}{\numberline {4.1.11}Instrumentaci\'on}{17}{subsection.4.1.11}
\contentsline {subsection}{\numberline {4.1.12}Protocolo y procedimiento de recolecci\'on de datos }{18}{subsection.4.1.12}
\contentsline {subsection}{\numberline {4.1.13}Procedimiento de medici\'on }{19}{subsection.4.1.13}
\contentsline {subsection}{\numberline {4.1.14}An\'alisis de Datos}{19}{subsection.4.1.14}
\contentsline {section}{\numberline {4.2}Informaci\'on Sobre la Replicaci\'on}{19}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Motivaci\'on para la realizaci\'on de la replicaci\'on}{19}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Nivel de interacci\'on con los experimentadores originales}{19}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}Cambios Respecto al Experimento Original}{20}{subsection.4.2.3}
\contentsline {subsubsection}{\numberline {4.2.3.1}Poblaci\'on experimental}{20}{subsubsection.4.2.3.1}
\contentsline {subsubsection}{\numberline {4.2.3.2}Factores y niveles de la repliaci\'on}{20}{subsubsection.4.2.3.2}
\contentsline {subsubsection}{\numberline {4.2.3.3}Dise\~no experimental de la replicaci\'on}{21}{subsubsection.4.2.3.3}
\contentsline {section}{Notas bibliogr\'aficas}{21}{subsubsection.4.2.3.3}
\contentsline {section}{En el pr\'oximo cap\IeC {\'\i }tulo}{21}{subsubsection.4.2.3.3}
\contentsline {part}{I\hspace {1em}Ap\'endices}{23}{part.1}
\contentsline {chapter}{\numberline {A}As\IeC {\'\i } se hizo...}{25}{appendix.Alph1}
\contentsline {section}{\numberline {A.1}Introducci\'on}{25}{section.Alph1.1}
\contentsline {part}{Bibliograf\IeC {\'\i }a}{27}{appendix*.10}
